module gitlab.com/travelter/travelgo

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/aws/aws-sdk-go v1.33.12 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/trifles v0.0.0-20200705224438-cafc02a1ee2b
	github.com/docker/go-units v0.4.0 // indirect
	github.com/getsentry/sentry-go v0.7.0
	github.com/gin-gonic/gin v1.6.3
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/kr/text v0.2.0 // indirect
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/mattn/go-shellwords v1.0.10 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/prisma/prisma-client-lib-go v0.0.0-20181017161110-68a1f9908416
	github.com/rs/xid v1.2.1
	github.com/secrethub/secrethub-go v0.30.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
	golang.org/x/sys v0.0.0-20200724161237-0e2f3a69832c // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
