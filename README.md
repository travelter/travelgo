# TravelGo - The Awesome Travelter Golang Methods Library
[![pipeline status](https://gitlab.com/travelter/travelgo/badges/master/pipeline.svg)](https://gitlab.com/travelter/travelgo/-/commits/master) [![coverage report](https://gitlab.com/travelter/travelgo/badges/master/coverage.svg)](https://gitlab.com/travelter/travelgo/-/commits/master)


We use this repo as a private go module collection

## Setup :

For UN*X like OSes :

- Create a personnal acess token for your gitlab profile [here](https://gitlab.com/profile/personal_access_tokens)

- You could set an `expiration date` if your kind of paranoid

- Give it only the `read_repository` right as it wont do much than that

- After hitting `Create personal acess token` copy the given value to clipboard

- Go back to your IDE ; Note : I, @dave-lopeur recommend you to save it in a .env file somewhere in the repo (see docker part below for .dev.env convention)

- Then open or create a file called `/home/$USER/.netrc` and insert the below lines 

```
machine gitlab.com
  login yourgitlabusername
  password youraccesstoken
```

- Finally go back to your Go Code and enter `go env -w GOPRIVATE=gitlab.com/travelter`

- Success !! You could now use "gitlab.com/travelter/travelgo" utilities in your code 

For windows :

- Follow the previous steps to get access token

- See this [SO answer](https://stackoverflow.com/a/45936697/13138128) to get the private package in go code

## Build `travelgo` base docker image

- Be sure to have a file called `.dev.env` on this repo's root

- Fill it like so :

```bash
CI_USER=urgitlabusername
CI_TOKEN=theaccesstokenyouhavepreviouslygenerated
```

- Run the makefile helper command :

```bash
make baseimage
```

- If successfull, you could now follow the [travelter_src](https://gitlab.com/travelter/travelter_src/-/blob/master/README.md) README to get the whole stack up and running 
