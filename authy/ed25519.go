package authy

import (
	"crypto/ed25519"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

var (
	// ErrEdDSAVerification is returned when an invalid signature is found
	ErrEdDSAVerification = fmt.Errorf("failed to validate EdDSA signature")
)

// SigningMethodEdDSA is the ed25519 implementation for jwt signing
type SigningMethodEdDSA struct{}

// Alg return the ed25519 name (EdDSA)
func (m *SigningMethodEdDSA) Alg() string {
	return "EdDSA"
}

// Verify check a jwt signingString against an EdDSA key
func (m *SigningMethodEdDSA) Verify(signingString string, signature string, key interface{}) error {
	var err error

	var sig []byte
	if sig, err = jwt.DecodeSegment(signature); err != nil {
		return err
	}

	var ed25519Key ed25519.PublicKey
	var ok bool
	if ed25519Key, ok = key.(ed25519.PublicKey); !ok {
		return jwt.ErrInvalidKeyType
	}

	if len(ed25519Key) != ed25519.PublicKeySize {
		return jwt.ErrInvalidKey
	}

	if ok := ed25519.Verify(ed25519Key, []byte(signingString), sig); !ok {
		return ErrEdDSAVerification
	}

	return nil
}

// Sign a string with an EdDSA private key
func (m *SigningMethodEdDSA) Sign(signingString string, key interface{}) (str string, err error) {
	var ed25519Key ed25519.PrivateKey
	var ok bool
	if ed25519Key, ok = key.(ed25519.PrivateKey); !ok {
		return "", jwt.ErrInvalidKeyType
	}

	if len(ed25519Key) != ed25519.PrivateKeySize {
		return "", jwt.ErrInvalidKey
	}

	// Sign the string and return the encoded result
	sig := ed25519.Sign(ed25519Key, []byte(signingString))
	return jwt.EncodeSegment(sig), nil
}
