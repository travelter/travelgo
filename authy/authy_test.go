package authy

import (
	"fmt"
	"strings"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
)

func TestLoad(t *testing.T) {
	var rt error
	var invalidCred = "keycred"
	var invalidPath = "keypath"
	var foo = AuthyConfig

	if rt = foo.Load(nil, nil); rt == nil || rt != ErrNoSecretHub {
		t.Error("expected ErrNoSecretHub got nil\n")
	}
	if rt = foo.Load(&invalidCred, nil); rt == nil || rt != ErrNoPubKeyPath {
		t.Error("expected ErrNoPubKeyPath got nil\n")
	}
	if rt = foo.Load(&invalidCred, &invalidPath); rt == nil || rt != ErrBadSecrets {
		t.Error("expected ErrBadSecrets got nil\n")
	}
	fmt.Printf("%s\n", rt.Error())

	godotenv.Load("../.dev.env", ".dev.env", "../dev_env")
	if rt = foo.Load(nil, nil); rt != nil {
		t.Errorf("unexpected  error : %s\n", rt.Error())
	}
	if rt = foo.Load(&invalidCred, &invalidPath); rt == nil {
		t.Error("expected ErrBadSecrets got nil\n")
	}
	// fmt.Printf("%v\n", foo)

}

func TestVerify(t *testing.T) {
	var rt error
	var testClaims jwt.StandardClaims
	var valid = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb2NhbGhvc3Q6QVVUSCIsImV4cCI6MTU5NDA1MjE3NSwianRpIjoiYnMxaGMzdGJobDhoZ2h1OWtydmciLCJpYXQiOjE1OTQwMzc3NzUsImlzcyI6InRyYXZlbHRlci1kZXYiLCJzdWIiOiJ1c2VyX21pY2hlbDpVU0VSIn0.rpZzFPJHuOWU2yBv5m-lUsC5yXI_Foims3qm78MvgCzefpOC6mjUdqXNXgqavtIJkH79ecMGlN1yl6CuFbxJ9ofrsv50aBfZ81nW7JXgIqzBU0qYD6aNuFue1Je_OC2UVCouWozyT46kwUHO1HOxrqNF43mQGnQBSEFH_pnQxnmDetU_DsyhdlYkeiM2baAXf_ScBF17WwfbP-mH-QyEv_HNt66-w-G5w0w9bkeIIil0aa3XgzwsofmHTLS6sj4HTkHdvjchqkoKWvENMjeGEJGIllz_MP0h1XyddYl2q0lfmkUotwd1HjdlD-wPtkAuZaLu9zzCCkCzlocxHmzLFdFwqiYj4i2DUFAcslzigbniKWV4kyR02To66oniQuxVLk68VQiTBFREPUg2cmuCtxms8Aj0ZEGURwakQcEmz85SeRTto0d4tTusKwxt8KRF83zokh6puo6tP3Os1uZLnp6ky3xk5vP4Rz6Ii2qo3y3NGfLX643K7LYSPs3w7fS7A7TKFdBeqj53PJ8Sci_i5D6f-308zVJiVQSmBE09yscHdXoyv-hUjP0B0qSwVyVTDqPc9we90OGddicSQ-ufMfbf5KKWLV2Jnvu1l9ft8wQIVi-AztOGUEUPkZD5eAF5r4Ws6xAg6twMhIBsmPRdenGUe3TisSR2x1ZUinxM87g"
	var testBadAlg = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb2NhbGhvc3Q6QVVUSCIsImV4cCI6MTU5NDA1MjE3NSwianRpIjoiYnMxaGMzdGJobDhoZ2h1OWtydmciLCJpYXQiOjE1OTQwMzc3NzUsImlzcyI6InRyYXZlbHRlci1kZXYiLCJzdWIiOiJ1c2VyX21pY2hlbDpVU0VSIn0.rpZzFPJHuOWU2yBv5m-lUsC5yXI_Foims3qm78MvgCzefpOC6mjUdqXNXgqavtIJkH79ecMGlN1yl6CuFbxJ9ofrsv50aBfZ81nW7JXgIqzBU0qYD6aNuFue1Je_OC2UVCouWozyT46kwUHO1HOxrqNF43mQGnQBSEFH_pnQxnmDetU_DsyhdlYkeiM2baAXf_ScBF17WwfbP-mH-QyEv_HNt66-w-G5w0w9bkeIIil0aa3XgzwsofmHTLS6sj4HTkHdvjchqkoKWvENMjeGEJGIllz_MP0h1XyddYl2q0lfmkUotwd1HjdlD-wPtkAuZaLu9zzCCkCzlocxHmzLFdFwqiYj4i2DUFAcslzigbniKWV4kyR02To66oniQuxVLk68VQiTBFREPUg2cmuCtxms8Aj0ZEGURwakQcEmz85SeRTto0d4tTusKwxt8KRF83zokh6puo6tP3Os1uZLnp6ky3xk5vP4Rz6Ii2qo3y3NGfLX643K7LYSPs3w7fS7A7TKFdBeqj53PJ8Sci_i5D6f-308zVJiVQSmBE09yscHdXoyv-hUjP0B0qSwVyVTDqPc9we90OGddicSQ-ufMfbf5KKWLV2Jnvu1l9ft8wQIVi-AztOGUEUPkZD5eAF5r4Ws6xAg6twMhIBsmPRdenGUe3TisSR2x1ZUinxM87g"
	// var testValidStringBadSigned = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gZGtkayIsImFkbWluIjp0cnVlLCJpYXQiOjE1MTYyMzkwMjJ9.ckw8wTB6kHa2MecpV_AdaJORy6e6mioGAlM5uN8t_DQ"
	// var testValidStringValidMethodBadSIgn = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.POstGetfAytaZS82wHcjoTyoqhMyxXiWdR7Nn7A29DNSl0EiXLdwJ6xC6AfgZWF1bOsS_TuYI3OG85AmiExREkrS6tDfTQ2B3WXlrr-wp5AokiRbz3_oB4OxG-W9KcEEbDRcZc0nH3L7LzYptiy1PtAylQGxHTWZXtGz4ht0bAecBgmpdgXMguEIcoqPJ1n3pIWk_dUZegpqx0Lka21H6XxUTxiy8OcaarA8zdnPUnV6AmNP3ecFawIFYdvJB_cm-GvpCSbr8G8y_Mllj8f4x9nBH8pQux89_6gUY618iYv7tuPWBFfEbLxtF2pZS6YC1aSfLQxeNe8djT9YjpvRZA"
	// auth
	AuthyConfig = Config{}
	fmt.Printf("Authy config 1 :\n%+v\n", AuthyConfig)
	if rt = Verify("", nil); rt != ErrInvalidKey {
		t.Error("unexpected nil error for bad key")
	}
	fmt.Printf("Have error [%s] want [%s]\n", rt.Error(), ErrInvalidKey)
	godotenv.Load("../.dev.env", ".dev.env")
	AuthyConfig.Load(nil, nil)
	if rt = Verify(valid, &testClaims); rt != nil && strings.Contains(rt.Error(), "is expired by") {
		t.Error("unexpected error for good (expired) string")
	}
	fmt.Printf("%+v\n", testClaims)
	if rt = Verify(testBadAlg, &testClaims); rt != ErrBadToken {
		t.Error("unexpected error for bad alg string")
	}
	// fmt.Printf("Have error [%s]\n", rt.Error())
}
