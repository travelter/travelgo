package authy

import (
	"crypto/rsa"
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/secrethub/secrethub-go/pkg/secrethub"
)

var (
	// ErrNoSecretHub is returned when secrethub credential are not found
	ErrNoSecretHub = fmt.Errorf("missing SECRETHUB_CREDENTIAL cannot start")
	// ErrNoPubKeyPath is returned when the needed env var related to the pub key path isnt set
	ErrNoPubKeyPath = fmt.Errorf("missing JWT_PUBKEY_PATH cannot start")
	// ErrBadAlgo is returned when the token alg method is invalid
	ErrBadAlgo = fmt.Errorf("invalid signature method")
	// ErrBadSecrets is an invalid secrethub vars
	ErrBadSecrets = fmt.Errorf("invalid secrethub credentials")
	// ErrInvalidKey is an rsa key error
	ErrInvalidKey = fmt.Errorf("invalid RSA signing key")
	// ErrBadToken is returned when token is invalid
	ErrBadToken = fmt.Errorf("invalid token string")
)

// Config is the needed config for authy to do his job
type Config struct {
	SecretHubCred       string
	SecretHubPubKeyPath string
	JWTPubKeyString     string
	JWTPUBKey           *rsa.PublicKey
}

// AuthyConfig is the global var to load secrets
var AuthyConfig = Config{}

// Load set the AuthyConfig global vars (could be in env or directly set with Load vars)
func (c *Config) Load(secretHubCredential, secretHubPubKeyPath *string) error {
	var (
		err    error
		client *secrethub.Client
	)
	if secretHubCredential == nil {
		envar := os.Getenv("SECRETHUB_CREDENTIAL")
		secretHubCredential = &envar
	}
	if secretHubPubKeyPath == nil {
		envar := os.Getenv("JWT_PUBKEY_PATH")
		secretHubPubKeyPath = &envar
	}
	if AuthyConfig.SecretHubCred = *secretHubCredential; AuthyConfig.SecretHubCred == "" {
		return ErrNoSecretHub
	}
	if AuthyConfig.SecretHubPubKeyPath = *secretHubPubKeyPath; AuthyConfig.SecretHubPubKeyPath == "" {
		return ErrNoPubKeyPath
	}

	client, err = secrethub.NewClient() // no need to check for error has none are returned for invalid creds

	if AuthyConfig.JWTPubKeyString, err = client.Secrets().ReadString(AuthyConfig.SecretHubPubKeyPath); err != nil {
		return ErrBadSecrets
	}
	if AuthyConfig.JWTPUBKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(AuthyConfig.JWTPubKeyString)); err != nil {
		return ErrInvalidKey
	}
	fmt.Printf("[Authy] %s | 🚀 Authy Succcessfully loaded\n", time.Now().Format(time.RFC3339))
	return nil
}

// Verify check a JWT token string (often found in Authorisation http header field) for validity
//  and return the deserialized claim structure if valid
func Verify(tokenString string, claims *jwt.StandardClaims) error {
	if AuthyConfig.JWTPUBKey == nil {
		return ErrInvalidKey
	}
	if _, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if token != nil && token.Method.Alg() != jwt.SigningMethodRS256.Name {
			return nil, ErrBadAlgo
		}
		return AuthyConfig.JWTPUBKey, nil
	}); err != nil {
		fmt.Printf("[Authy] %s | Invalid token : %s\n", time.Now().Format(time.RFC3339), err.Error())
		return ErrBadToken
	}
	return nil
}
