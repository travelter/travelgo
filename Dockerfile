############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder
# Install git to get go deps
RUN apk update && apk add --no-cache curl git ca-certificates tzdata && update-ca-certificates
# Setup Go private package
ARG CI_USER=${CI_USER}
ARG CI_TOKEN=${CI_TOKEN}
ENV CI_TOKEN=$CI_TOKEN \
    CI_USER=$CI_USER
RUN echo -e "\n\
machine gitlab.com\n\
  login $CI_USER\n\
  password $CI_TOKEN\n\
" >> /root/.netrc
# Set GO Build env var
ENV GOPRIVATE=gitlab.com/travelter/* \
    GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
# Create appuser.
ENV USER=appuser
ENV UID=10001 
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"
# Create build dir
WORKDIR ${GOPATH}/src/gitlab.com/travelter/travelgo
# COPY go.mod and go.sum files to the workspace
COPY go.mod go.sum ./
# Caching vendored deps
RUN go mod download
# Check their integrity
RUN go mod verify
# COPY the source code as the last step
COPY . .
