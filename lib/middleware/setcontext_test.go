package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestSetContext(t *testing.T) {
	r := gin.Default()
	// var valid = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb2NhbGhvc3Q6QVVUSCIsImV4cCI6MTU5NDA1MjE3NSwianRpIjoiYnMxaGMzdGJobDhoZ2h1OWtydmciLCJpYXQiOjE1OTQwMzc3NzUsImlzcyI6InRyYXZlbHRlci1kZXYiLCJzdWIiOiJ1c2VyX21pY2hlbDpVU0VSIn0.rpZzFPJHuOWU2yBv5m-lUsC5yXI_Foims3qm78MvgCzefpOC6mjUdqXNXgqavtIJkH79ecMGlN1yl6CuFbxJ9ofrsv50aBfZ81nW7JXgIqzBU0qYD6aNuFue1Je_OC2UVCouWozyT46kwUHO1HOxrqNF43mQGnQBSEFH_pnQxnmDetU_DsyhdlYkeiM2baAXf_ScBF17WwfbP-mH-QyEv_HNt66-w-G5w0w9bkeIIil0aa3XgzwsofmHTLS6sj4HTkHdvjchqkoKWvENMjeGEJGIllz_MP0h1XyddYl2q0lfmkUotwd1HjdlD-wPtkAuZaLu9zzCCkCzlocxHmzLFdFwqiYj4i2DUFAcslzigbniKWV4kyR02To66oniQuxVLk68VQiTBFREPUg2cmuCtxms8Aj0ZEGURwakQcEmz85SeRTto0d4tTusKwxt8KRF83zokh6puo6tP3Os1uZLnp6ky3xk5vP4Rz6Ii2qo3y3NGfLX643K7LYSPs3w7fS7A7TKFdBeqj53PJ8Sci_i5D6f-308zVJiVQSmBE09yscHdXoyv-hUjP0B0qSwVyVTDqPc9we90OGddicSQ-ufMfbf5KKWLV2Jnvu1l9ft8wQIVi-AztOGUEUPkZD5eAF5r4Ws6xAg6twMhIBsmPRdenGUe3TisSR2x1ZUinxM87g"
	r.Use(SetContext())
	ts := httptest.NewServer(r)
	defer ts.Close()
	// Make a request to our server with the {base url}/ping
	resp, err := http.Get(ts.URL)
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 404, got %v", resp.StatusCode)
	}

	req, err := http.NewRequest(http.MethodOptions, ts.URL, nil)
	req.Header.Set("Authorization", "bidon")
	res, err := http.DefaultClient.Do(req)
	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}
	req, err = http.NewRequest(http.MethodOptions, ts.URL, nil)
	req.Header.Set("Authorization", "Bearer bidon")
	res, err = http.DefaultClient.Do(req)
	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}
	// req, err = http.NewRequest(http.MethodOptions, ts.URL, nil)
	// req.Header.Set("Authorization", valid)
	// res, err = http.DefaultClient.Do(req)
	// if res.StatusCode != http.StatusNotFound {
	// 	t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	// }
}
