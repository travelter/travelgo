package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestCORSMiddleware(t *testing.T) {
	r := gin.Default()
	r.Use(CORSMiddleware())
	ts := httptest.NewServer(r)
	defer ts.Close()
	// Make a request to our server with the {base url}/ping
	resp, err := http.Get(ts.URL)
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected status code 404, got %v", resp.StatusCode)
	}

	req, err := http.NewRequest(http.MethodOptions, ts.URL, nil)
	res, err := http.DefaultClient.Do(req)
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}
}
