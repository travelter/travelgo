package middleware

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib"
	"gitlab.com/travelter/travelgo/lib/authcontext"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

var (
	localIP                   = "127.0.0.1"
	devMockIP                 = "90.5.218.242"
	bearerRegx                = regexp.MustCompile(`(?i)bearer`)
	errInvalidNumberOfSegment = errors.New("Token contains an invalid number of segments")
)

// SetContext get the Authorization header field, get the jwt and set the recorded value in context
func SetContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			debug      = os.Getenv("DEBUG") == "true"
			ctx        = c.Request.Context()
			claims     jwt.StandardClaims
			authHeader = c.GetHeader("Authorization")
			dbOption   = &prisma.Options{
				Endpoint: lib.GetDefVal("PRISMA_ENDPOINT", prisma.DefaultEndpoint),
			}
			db          = prisma.New(dbOption)
			authContext = authcontext.Context{
				UserIP:    c.ClientIP(),
				UserAgent: c.GetHeader("User-Agent"),
			}
		)
		if authContext.UserIP == localIP {
			authContext.UserIP = devMockIP
		}
		if debug {
			lib.LogInfo("midleware/SetContext", "DB : "+dbOption.Endpoint)
		}
		if authHeader != "" {
			if bearerRegx.Match([]byte(authHeader)) {
				authHeader = bearerRegx.ReplaceAllString(authHeader, "")
				authHeader = strings.Trim(authHeader, " ")
			}
			parsed, err := jwt.ParseWithClaims(authHeader, &claims, authcontext.CheckJWTSign)
			if err == nil {
				userInfos := strings.Split(claims.Subject, ":")
				subjectInfos := strings.Split(claims.Audience, ":")
				if parsed.Valid && len(userInfos) >= 1 && len(subjectInfos) == 2 {
					if debug {
						fmt.Printf("Infos || sub : %v, aud : %v\n", userInfos, subjectInfos)
					}
					if usr, err := db.User(prisma.UserWhereUniqueInput{
						Name: &userInfos[0],
					}).Exec(ctx); err == nil {
						authContext.UserName = usr.Name
						authContext.UserRole = string(usr.Role)
						authContext.AuthContext = subjectInfos[1]
						ctx = authcontext.NewContext(ctx, &authContext)
						if debug {
							lib.LogInfo("middleware/SetContext", fmt.Sprintf("Welcome %+v", authContext))
						}
					} else {
						lib.LogError("middleware/SetContext", err.Error())
					}
				} else {
					logmsg := fmt.Sprintf("Weird token : %+v\n", claims)
					lib.LogError("middleware/SetContext", logmsg)
				}
			} else if err.Error() != errInvalidNumberOfSegment.Error() {
				lib.LogError("middleware/SetContext", err.Error())
			}
		}
		ctx = authcontext.NewContext(ctx, &authContext)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}
