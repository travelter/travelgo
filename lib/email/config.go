package email

import (
	"net/smtp"

	"gitlab.com/travelter/travelgo/lib"
)

// SMTPAuth represent the smtp server credentials
type SMTPAuth struct {
	Host     string `toml:"smtp_host"`
	Port     string `toml:"smtp_port"`
	UserName string `toml:"smtp_user"`
	Password string `toml:"smtp_pass"`
	Md5Auth  smtp.Auth
}

// Config represent the needed config for email package
type Config struct {
	SMTPAuth
	SMTPFromField string `env:"SMTP_FROM"`
	MailDomain    string `env:"MAIL_DOMAIN"`
	SupportEmail  string `env:"SUPPORT_EMAIL"`
}

// EmailConf is the exported gloabal config object
var EmailConf = Config{}

// Load config from env var
// (must be called before everything related to mail sending task)
func (c *Config) Load() {
	c.Host = lib.GetDefVal("SMTP_HOST", "smtp.mailtrap.io")
	c.Port = lib.GetDefVal("SMTP_PORT", "2525")
	c.UserName = lib.GetDefVal("SMTP_USER", "371cdaf244fb46")
	c.Password = lib.GetDefVal("SMTP_PASS", "cd54c3cb439c15")
	c.SMTPFromField = lib.GetDefVal("SMTP_FROM", "Travelter <no-reply@mytravelter.com>")
	c.MailDomain = lib.GetDefVal("MAIL_DOMAIN", "mytravelter.com")
	c.SupportEmail = lib.GetDefVal("SUPPORT_EMAIL", "support@mytravelter.com")
	c.Md5Auth = smtp.CRAMMD5Auth(c.UserName, c.Password)
}
