package email

import (
	"fmt"
	"net/smtp"
	"strings"
	"time"

	"gitlab.com/travelter/travelgo/lib"

	"github.com/dgryski/trifles/uuid"
)

// Email represent a mail to send
type Email struct {
	To      []string
	From    string
	Subject string
	Message string
}

// SendEmail send and email with go smtp package
func SendEmail(email Email) chan error {
	const CRLF = "\r\n"
	const rfc2822 = "Mon, 02 Jan 2006 15:04:05 -0700"
	var errs = make(chan error, 1)
	defer close(errs)
	mime := "MIME-version: 1.0;" + CRLF
	tomsg := "To: " + strings.Join(email.To, ",") + CRLF
	frommsg := "From: " + email.From + CRLF
	subjectmsg := "Subject: " + email.Subject + CRLF
	date := "Date: " + time.Now().Format(rfc2822) + CRLF
	messageID := "Message-ID: <" + uuid.UUIDv4() + "@" + EmailConf.MailDomain + ">" + CRLF
	contentTransferEncoding := "Content-Transfer-Encoding: 8bit" + CRLF
	contentType := "Content-Type: text/html; charset=\"UTF-8\";" + CRLF
	fullBody := fmt.Sprintf("%s%s%s%s%s%s%s%s%s",
		date,
		tomsg,
		frommsg,
		subjectmsg,
		messageID,
		mime,
		contentTransferEncoding,
		contentType,
		email.Message,
	)
	logmsg := fmt.Sprintf("Will send mail size %v via %s:%s from %s to %s",
		len(email.Message),
		EmailConf.Host,
		EmailConf.Port,
		email.From,
		email.To,
	)
	lib.LogInfo("SendEmail", logmsg)
	if err := smtp.SendMail(
		EmailConf.Host+":"+EmailConf.Port,
		EmailConf.Md5Auth,
		EmailConf.UserName,
		email.To,
		[]byte(fullBody)); err != nil {
		lib.LogError("email/Send", err.Error())
		errs <- err
	}
	return errs
}
