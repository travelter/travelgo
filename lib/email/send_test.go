package email

import (
	"fmt"
	"testing"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

func TestSendEmail(t *testing.T) {
	EmailConf.Load()
	from := "{{.Fii}}@foo.com"
	pim, _ := TemplateToEmail(&prisma.EmailTemplate{
		Subject: "{{.Fii}}",
		Body:    "{{.Fii}}",
		From:    &from,
	}, struct{ Fii string }{Fii: "foo"})
	pim.To = []string{"sikiy29084@aqumail.com"}
	fofof := SendEmail(*pim)
	msg := <-fofof
	fmt.Println(msg)
}
