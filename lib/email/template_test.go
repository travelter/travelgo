package email

import (
	"testing"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

func TestFillTemplate(t *testing.T) {
	fii, foo := FillTemplate("{{.Test}}", struct{ Test string }{Test: "value"})
	if foo != nil {
		t.Logf("Unexpected error %s\n", foo.Error())
	}
	if fii != "value" {
		t.Logf("want 'value' have '%s'\n", fii)
	}
	fii, foo = FillTemplate("{{.est}}", struct{ Test string }{Test: "value"})
	if foo == nil {
		t.Log("Unexpected nil error\n")
	}
}

func TestTemplateToEmail(t *testing.T) {
	from := "{{.Fii}}@foo.com"
	pim, pam := TemplateToEmail(&prisma.EmailTemplate{
		Subject: "{{.Fii}}",
		Body:    "{{.Fii}}",
		From:    &from,
	}, struct{ Fii string }{Fii: "foo"})
	if pam != nil {
		t.Logf("want nil got error %s\n", pam.Error())
	}
	if pim.Subject != "foo" {
		t.Logf("Err want 'foo' got '%s'", pim.Subject)
	}
	if pim.Message != "foo" {
		t.Logf("Err want 'foo' got '%s'", pim.Message)
	}
	if pim.From != "foo@foo.com" {
		t.Logf("Err want 'foo' got '%s'", pim.Message)
	}
}
