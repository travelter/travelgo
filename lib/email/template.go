package email

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib"
)

var (
	// ErrParseTemplate is returned when go template failed to parse a '{{.Var}}' type string
	ErrParseTemplate = fmt.Errorf("failed to parse template")
	// ErrFillTemplate is returned when there is trouble replacing {Var: "value"} in `{{.Var}}` template
	ErrFillTemplate = fmt.Errorf("failed to fill template")
)

// ValidationEmailParams is the needed param to fill a registration email template
type ValidationEmailParams struct {
	Username     string
	ConfirmLink  string
	SupportEmail string
}

// FillTemplate return a template string filled with the required variables
func FillTemplate(tofill string, params interface{}) (string, error) {
	var (
		err    error
		result string
		tpl    bytes.Buffer
		t      *template.Template
	)
	if t, err = template.New("confirm_email").Parse(tofill); err != nil {
		log := fmt.Sprintf("Failed to parse template : %s\n", err.Error())
		lib.LogError("mail/FillTemplate", log)
		return result, ErrFillTemplate
	}
	if err = t.Execute(&tpl, params); err != nil {
		log := fmt.Sprintf("Failed to fill template : %s\n", err.Error())
		lib.LogError("mail/FillTemplate", log)
		return result, ErrParseTemplate
	}
	result = tpl.String()
	return result, err
}

// TemplateToEmail fill an email template with params and return an email ready to send
func TemplateToEmail(tpl *prisma.EmailTemplate, params interface{}) (*Email, error) {
	var err error
	var filled string
	var nemail = Email{
		Subject: tpl.Subject,
		Message: tpl.Body,
		From:    EmailConf.SMTPFromField,
	}
	switch {
	case strings.Contains(tpl.Subject, "{{"):
		if filled, err = FillTemplate(tpl.Subject, params); err == nil && filled != "" {
			nemail.Subject = filled
		}
	case strings.Contains(tpl.Body, "{{"):
		if filled, err = FillTemplate(tpl.Body, params); err == nil && filled != "" {
			nemail.Message = filled
		}
	case tpl.From != nil && strings.Contains(*tpl.From, "{{"):
		if filled, err = FillTemplate(*tpl.From, params); err == nil && filled != "" {
			nemail.From = filled
		}
	}
	return &nemail, err
}
