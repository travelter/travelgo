package lib

import (
	"fmt"
)

const (
	// DevStage is the dev stage identifier
	DevStage = "dev"
)

// Config represent the graphql_server configuration variables
type Config struct {
	Stage                  string `env:"STAGE"`
	Release                string `env:"RELEASE"`
	Port                   string `env:"PORT"`
	Host                   string `env:"HOST"`
	URL                    string `env:"URL"`
	DBURL                  string `env:"DB_URL"`
	MailConfirmRedirectURL string `env:"MAIL_REDIRECT"`
	GeoIPApiURL            string `env:"GEOIP_URL"`
	SentryDSN              string `env:"SENTRY_DSN"`
	CorsAllowedOrigin      string `env:"CORS_ALLOWED_ORIGINS"`
}

// ServerConf is the exported config object singleton
var ServerConf = Config{}

// Load config options from environnement variables
func (c *Config) Load() {
	c.Stage = GetDefVal("STAGE", DevStage)
	c.Release = GetDefVal("RELEASE", "v1.1.8")
	c.Port = GetDefVal("PORT", "4002")
	c.Host = GetDefVal("HOST", "localhost")
	c.URL = GetDefVal("URL", fmt.Sprintf("http://%s:%s", c.Host, c.Port))
	c.MailConfirmRedirectURL = GetDefVal("MAIL_REDIRECT", "http://localhost:3000")
	c.GeoIPApiURL = GetDefVal("GEOIP_API", "https://freegeoip.app/json")
	c.SentryDSN = GetDefVal("SENTRY_DSN", "https://67f58ee98e4e4b76a50ffc280a8eaaf1@sentry.io/5176087")
	c.CorsAllowedOrigin = GetDefVal("CORS_ALLOWED_ORIGINS", "*")
}
