package authcontext

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

var (
	// ErrBadAlgo is returned wen the 'alg' field of the jwt header is not the one
	// we sign this token with
	ErrBadAlgo = fmt.Errorf("invalid alg value")
	// ErrBadToken is returned when the provided jwtToken is incorrect
	ErrBadToken = fmt.Errorf("bad jwt token")
)

// CheckJWTSign return the signing bytes for the jwt lib to validate token signature
func CheckJWTSign(token *jwt.Token) (interface{}, error) {
	var err error
	if token == nil {
		err = ErrBadToken
	} else if token.Method.Alg() != jwt.SigningMethodRS256.Name {
		err = ErrBadAlgo
	}
	return SigningConfig.JwtSigningPubKey, err
}
