package authcontext

import (
	"testing"

	"github.com/joho/godotenv"
)

func TestInit(t *testing.T) {
	var foo = SigningConfig
	godotenv.Load("../../.dev.env", ".dev.env", "../dev_env", "../../dev_env")
	foo.Init()
}
