package authcontext

import (
	"testing"

	"github.com/dgrijalva/jwt-go"
)

func TestCheckJWTSign(t *testing.T) {
	// var valid = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb2NhbGhvc3Q6QVVUSCIsImV4cCI6MTU5NDA1MjE3NSwianRpIjoiYnMxaGMzdGJobDhoZ2h1OWtydmciLCJpYXQiOjE1OTQwMzc3NzUsImlzcyI6InRyYXZlbHRlci1kZXYiLCJzdWIiOiJ1c2VyX21pY2hlbDpVU0VSIn0.rpZzFPJHuOWU2yBv5m-lUsC5yXI_Foims3qm78MvgCzefpOC6mjUdqXNXgqavtIJkH79ecMGlN1yl6CuFbxJ9ofrsv50aBfZ81nW7JXgIqzBU0qYD6aNuFue1Je_OC2UVCouWozyT46kwUHO1HOxrqNF43mQGnQBSEFH_pnQxnmDetU_DsyhdlYkeiM2baAXf_ScBF17WwfbP-mH-QyEv_HNt66-w-G5w0w9bkeIIil0aa3XgzwsofmHTLS6sj4HTkHdvjchqkoKWvENMjeGEJGIllz_MP0h1XyddYl2q0lfmkUotwd1HjdlD-wPtkAuZaLu9zzCCkCzlocxHmzLFdFwqiYj4i2DUFAcslzigbniKWV4kyR02To66oniQuxVLk68VQiTBFREPUg2cmuCtxms8Aj0ZEGURwakQcEmz85SeRTto0d4tTusKwxt8KRF83zokh6puo6tP3Os1uZLnp6ky3xk5vP4Rz6Ii2qo3y3NGfLX643K7LYSPs3w7fS7A7TKFdBeqj53PJ8Sci_i5D6f-308zVJiVQSmBE09yscHdXoyv-hUjP0B0qSwVyVTDqPc9we90OGddicSQ-ufMfbf5KKWLV2Jnvu1l9ft8wQIVi-AztOGUEUPkZD5eAF5r4Ws6xAg6twMhIBsmPRdenGUe3TisSR2x1ZUinxM87g"
	// var foo = GetJwtString(30*time.Second, "FII:FOO", false)
	// jwt.Parse(valid, CheckJWTSign)
	// CheckJWTSign(&jwt.Token{})
	if _, err := CheckJWTSign(&jwt.Token{
		Method: jwt.SigningMethodES256,
	}); err != ErrBadAlgo {
		t.Logf("Want errBadalgo got %s", err.Error())
	}

}
