package authcontext

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/travelter/travelgo/lib"

	"github.com/dgrijalva/jwt-go"
	"github.com/secrethub/secrethub-go/pkg/secrethub"
)

// AuthConfig is the needed var to grant authorization tokens
type AuthConfig struct {
	JwtIssuer              string `env:"JWT_ISSUER"`
	JwtSigningPubKeyString string `env:"SIGN_KEY"`
	JWTPubKeyPath          string `env:"JWT_PUBKEY_PATH"`
	JwtSigninPrivKeyString string `env:"PRIV_KEY"`
	JWTPrivKeyPassword     string `env:"PRIV_KEY_PASS"`
	JWTPrivKeyPath         string `env:"JWT_PRIVKEY_PATH"`
	TotpExpiration         string `env:"TOTP_EXP"`
	SessionExpiration      string `env:"SESSION_EXP"`
	SecretHubCredential    string `env:"SECRETHUB_CREDENTIAL"`
	JwtSigningPubKey       *rsa.PublicKey
	JwtSigninPrivKey       *rsa.PrivateKey
	TotpDuration           time.Duration
	SessionDuration        time.Duration
}

var (
	// ErrBadSecrets is a secrethub services setup failure
	ErrBadSecrets = fmt.Errorf("invalid secrethub credentials")
	// ErrInvalidKey is an unparsable key error
	ErrInvalidKey = fmt.Errorf("invalid signing key")
	// ErrBadDuration is returned when an invalid duration param is found
	ErrBadDuration = fmt.Errorf("invalid expiration value")
)

// SigningConfig is the global var for jwt token signing
var SigningConfig = AuthConfig{}

// Init populate the global package config struct with en vars or default
func (c *AuthConfig) Init() error {
	var err error
	c.JwtIssuer = lib.GetDefVal("JWT_ISSUER", "travelter-dev")
	c.JwtSigningPubKeyString = lib.GetDefVal("SIGN_KEY", "./my.key.pub")
	c.JwtSigninPrivKeyString = lib.GetDefVal("PRIV_kEY", "./my.key.pem")
	c.JWTPrivKeyPassword = lib.GetDefVal("PRIV_KEY_PASS", "")
	c.TotpExpiration = lib.GetDefVal("TOTP_EXP", "15m")
	c.SessionExpiration = lib.GetDefVal("SESSION_EXP", "12h")
	c.SecretHubCredential = lib.GetDefVal("SECRETHUB_CREDENTIAL", "")
	c.JWTPubKeyPath = lib.GetDefVal("JWT_PUBKEY_PATH", "")
	c.JWTPrivKeyPath = lib.GetDefVal("JWT_PRIVKEY_PATH", "")
	if os.Getenv("STAGE") == lib.DevStage {
		pubkeyfile, _ := ioutil.ReadFile(c.JwtSigningPubKeyString)
		privkeyfile, _ := ioutil.ReadFile(c.JwtSigninPrivKeyString)
		c.JwtSigningPubKeyString = string(pubkeyfile)
		c.JwtSigninPrivKeyString = string(privkeyfile)
	} else {
		client, _ := secrethub.NewClient()
		if c.JwtSigningPubKeyString, err = client.Secrets().ReadString(c.JWTPubKeyPath); err != nil {
			lib.LogError("authcontext/config", err.Error())
			// os.Exit(1)
			return ErrBadSecrets
		}
		if c.JwtSigninPrivKeyString, err = client.Secrets().ReadString(c.JWTPrivKeyPath); err != nil {
			lib.LogError("authcontext/config", err.Error())
			// os.Exit(1)
			return ErrBadSecrets
		}
	}

	if c.JwtSigningPubKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(c.JwtSigningPubKeyString)); err != nil {
		lib.LogError("authcontext/config", err.Error())
		// os.Exit(1)
		return ErrInvalidKey
	}
	if c.JwtSigninPrivKey, err = jwt.ParseRSAPrivateKeyFromPEMWithPassword(
		[]byte(c.JwtSigninPrivKeyString),
		c.JWTPrivKeyPassword,
	); err != nil {
		lib.LogError("authcontext/config", err.Error())
		// os.Exit(1)
		return ErrInvalidKey
	}
	if c.TotpDuration, err = time.ParseDuration(c.TotpExpiration); err != nil {
		lib.LogError("lib/Config", err.Error())
		// os.Exit(1)
		return ErrBadDuration
	}
	if c.SessionDuration, err = time.ParseDuration(c.SessionExpiration); err != nil {
		lib.LogError("lib/Config", err.Error())
		// os.Exit(1)
		return ErrBadDuration
	}
	lib.LogInfo("authcontext/config", "conf sucessfuly loaded")
	return err
}
