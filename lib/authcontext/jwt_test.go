package authcontext

import (
	"testing"
	"time"

	"github.com/joho/godotenv"
)

func TestGetJwtString(t *testing.T) {
	godotenv.Load("../../.dev.env", ".dev.env", "../dev_env", "../../dev_env")
	var fii = GetJwtString(30*time.Second, "FII:FOO", false)
	if fii != nil {
		t.Errorf("Want error got %s\n", *fii)
	}

	if err := SigningConfig.Init(); err != nil {
		t.Errorf("failed to setup test got err %s\n", err.Error())
	}

	fii = GetJwtString(30*time.Second, "FII:FOO", false)
	if fii == nil {
		t.Errorf("Want string got nil")
	}

	fii = GetJwtString(30*time.Second, "FII:FOO", true)
	if fii == nil {
		t.Errorf("Want string got nil")
	}

	SigningConfig.JwtSigninPrivKey = nil
	fii = GetJwtString(30*time.Second, "FII:FOO", false)
	if fii != nil {
		t.Errorf("Want error got %s\n", *fii)

	}

}
