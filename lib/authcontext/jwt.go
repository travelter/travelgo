package authcontext

import (
	"time"

	"gitlab.com/travelter/travelgo/lib"

	"github.com/dgrijalva/jwt-go"
	"github.com/rs/xid"
)

const (
	// OTPAudience represent a token granted for an otp confirmation session
	OTPAudience = "OTP"
	// AuthAudience represent a token granted for a full authenticated session
	AuthAudience = "AUTH"
)

// GetJwtString return a signed token with exp set in hours
func GetJwtString(exp time.Duration, sub string, otp bool) *string {
	var err error
	var token *jwt.Token
	var tokenString string
	expirationTime := time.Now().Add(exp)
	audience := lib.ServerConf.Host + ":"
	jID := xid.NewWithTime(time.Now()).String()
	if otp {
		audience += OTPAudience
	} else {
		audience += AuthAudience
	}
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token = jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.StandardClaims{
		Issuer:    SigningConfig.JwtIssuer,
		Audience:  audience,
		Subject:   sub,
		ExpiresAt: expirationTime.Unix(),
		IssuedAt:  time.Now().Unix(),
		Id:        jID,
	})
	if SigningConfig.JwtSigninPrivKey == nil {
		lib.LogError("authcontext/GetJwtString", "Signing config not set")
		return nil
	}
	// Sign and get the complete encoded token as a string using the secret
	key := SigningConfig.JwtSigninPrivKey
	if tokenString, err = token.SignedString(key); err != nil {
		lib.LogError("lib/GetJwtString", err.Error())
	}
	return &tokenString
}
