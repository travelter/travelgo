package authcontext

import "context"

// Context represent the request context infos
type Context struct {
	UserIP      string `json:"user_ip"`
	UserAgent   string `json:"user_agent"`
	UserName    string `json:"user_name,omitempty"`
	UserRole    string `json:"user_role,omitempty"`
	AuthContext string `json:"auth_context,omitempty"`
}

type key int

var contextKey key

// NewContext returns a new Context that carries value u.
func NewContext(ctx context.Context, u *Context) context.Context {
	return context.WithValue(ctx, contextKey, u)
}

// FromContext returns the User value stored in ctx, if any.
func FromContext(ctx context.Context) (*Context, bool) {
	u, ok := ctx.Value(contextKey).(*Context)
	return u, ok
}
