package lib

import (
	"testing"

	"github.com/joho/godotenv"
)

func TestGetDefVal(t *testing.T) {
	var fii = GetDefVal("nope", "val")
	if fii != "val" {
		t.Errorf("want 'val' got %s\n", fii)
	}

	godotenv.Load("../.dev.env", ".dev.env")
	fii = GetDefVal("KEY", "nope")
	if fii != "val" {
		t.Errorf("want 'val' got %s\n", fii)
	}
}
