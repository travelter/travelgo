package lib

import "testing"

func TestInitSentry(t *testing.T) {
	InitSentry()
}

func TestLogInfo(t *testing.T) {
	LogInfo("test", "test")
}

func TestLogError(t *testing.T) {
	LogError("test", "test")
}
