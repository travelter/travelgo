package geoip

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/travelter/travelgo/lib"
	"gitlab.com/travelter/travelgo/lib/authcontext"
)

// GeoIP is the response from freegeoip
type GeoIP struct {
	IP          string  `json:"ip"`
	CountryCode string  `json:"country_code"`
	CountryName string  `json:"country_name"`
	RegionCode  string  `json:"region_code"`
	RegionName  string  `json:"region_name"`
	City        string  `json:"city"`
	Zipcode     string  `json:"zipcode"`
	Lat         float64 `json:"latitude"`
	Lon         float64 `json:"longitude"`
	MetroCode   int     `json:"metro_code"`
	AreaCode    int     `json:"area_code"`
}

var (
	response *http.Response
	body     []byte
)

// GetGeoip return MaxMind db data for an ip address
func GetGeoip(reqaddress string, withauth bool) *GeoIP {
	var err error
	var geo GeoIP
	var client = &http.Client{}
	var nRequest *http.Request
	var reqURL = lib.ServerConf.GeoIPApiURL + "/" + reqaddress

	if nRequest, err = http.NewRequest("GET", reqURL, nil); err != nil {
		lib.LogError("geoip/GetGeoip", err.Error())
		return nil
	}
	if withauth {
		autHeaderValue := authcontext.GetJwtString(1*time.Hour, "auth_server:ADMIN", false)
		if autHeaderValue == nil {
			lib.LogError("geoip/GetGeoip", "Failed to get a jwt string")
			return nil
		}
		nRequest.Header.Add("Authorization", *autHeaderValue)
	}
	if response, err = client.Do(nRequest); err != nil {
		lib.LogError("geoip/GetGeoip", err.Error())
		return nil
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return nil
	}
	body, err = ioutil.ReadAll(response.Body)
	if err != nil && lib.ServerConf.Stage != lib.DevStage {
		lib.LogError("geoip/GetGeoip", err.Error())
		return nil
	}
	err = json.Unmarshal(body, &geo)
	if err != nil && lib.ServerConf.Stage != lib.DevStage {
		lib.LogError("geoip/GetGeoip", err.Error())
		return nil
	}
	return &geo
}
