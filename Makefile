####
## An auth_serverGoGraphql API Makefile
## (run $ make for help)
####
.PHONY: help start


RM			= rm -f
REPORTS     ?= reports
PWD         := $(shell pwd)
LIB_NAME 	:= $(shell head -n 1 README.md | cut -d ' ' -f2 |  tr '[:upper:]' '[:lower:]')
LIB_VSN 	:= $(shell git describe --tags | cut -d '-' -f1 || 'vdev')
BUILD 		:= $(shell git rev-parse --short HEAD)

SHELL 				:= /bin/bash

PORT        		?= 4000

STAGE        		?= dev
ENVFILE      		= .$(strip $(STAGE)).env
MODULE_LIST			:= $(shell go list ./... | grep -v generated)

CI_USER             := $(shell source $(ENVFILE) 2&>/dev/null ; echo $$CI_USER)
CI_TOKEN            := $(shell source $(ENVFILE) 2&>/dev/null ; echo $$CI_TOKEN)

help: ## Print this help message and exit
	@echo -e "\n\t$(LIB_NAME):$(APP_VSN)-$(BUILD) \033[1mmake\033[0m options:\n"
	@perl -nle 'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "- \033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo -e "\n"

default: help

testsetup:
	@cd ../geoip && go run . &
	@echo "geoip API started"

testteardown:
	@killall geoip

baseimage:
	@docker build \
		--build-arg CI_USER=$(CI_USER) --build-arg CI_TOKEN=$(CI_TOKEN) \
		-t $(LIB_NAME):latest .

test: ## Run the go test command with coverage
	@go test `go list ./... | grep -v generated` -cover
	@cd authy && go test . -cover

lint: ## Run golangci-lint sast scanning (see https://github.com/golangci/golangci-lint for documentation)
	@mkdir -p $(REPORTS) &2>/dev/null || true
	@docker run --rm -v $(shell pwd):/app -w /app golangci/golangci-lint:v1.26.0 golangci-lint run -v &>$(REPORTS)/golangci_report.txt || true

godoc: ## Show all documentation on all go modules
	@for i in $(MODULE_LIST); do go doc -all $$i \
		| sed '2s/^/# /' &>>$(REPORTS)/doc.md; done

restart: ## Stop and start in dev mode
	@make stop
	@make serve

codegen: ## Getting gqlgen to work (to be run after updates on schema/*.gql files)
	@prisma generate
	@echo -e "\n🔧  Applying fix for Uuid type issue (see https://github.com/prisma/prisma/issues/4438 )"
	@sed -i '15 a type Uuid string\n' ./generated/prisma/prisma.go
	@echo -e "\n🔧 Adding Apollo Federation Required interface User.IsEntity()"
	@sed -i '/^type DeviceExec struct*/i func (User) IsEntity() {}\n' ./generated/prisma/prisma.go
	@echo "Et voila ! 🦊"
